import React from 'react';

const ErrorComponent = () => {
  const errorStyle = {
    color: 'red',
    padding: '10px',
    display: 'flex',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  };

  return (
    <div style={errorStyle}>
      <p>There is some errror. Please try refreshing page</p>
    </div>
  );
};

export default ErrorComponent;
