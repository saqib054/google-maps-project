import React from 'react';

const Spinner = () => {
  const spinnerContainer = {
    padding: '10px',
    display: 'flex',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  };

  const spinnerStyle = {
    color: 'blue'
  };

  return (
    <div style={spinnerContainer}>
      <span style={spinnerStyle}>
        <i className="fas fa-spinner fa-spin fa-4x" />
      </span>
    </div>
  );
};

export default Spinner;
