import React, { useState, useEffect, memo } from 'react';
import GoogleMapReact from 'google-map-react';

import InfoWindow from './InfoWindow';
import Marker from './Marker';
import ErrorComponent from './ErrorComponent';
import Spinner from './Spinner';

const mapContainer = {
  width: '100vw',
  height: '100vh'
};

const defaultCenter = {
  lat: 56.26392,
  lng: 9.501785
};

const defaultZoom = 10;

const Map = () => {
  const [users, setUsers] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  const fetchUsers = async () => {
    try {
      setLoading(true);
      let users = await fetch('http://localhost:3000/users');
      users = await users.json();
      setUsers(users);
    } catch (error) {
      setError(true);
    } finally {
      setLoading(false);
    }
  };

  const openInfoWindow = user => {
    setSelectedUser(user);
  };

  const closeInfoWindow = () => {
    setSelectedUser(null);
  };

  useEffect(() => {
    fetchUsers();
  }, []);

  return (
    <div>
      {loading && <Spinner />}
      {!error && !loading && (
        <div style={mapContainer}>
          <GoogleMapReact
            bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_KEY }}
            defaultZoom={defaultZoom}
            defaultCenter={defaultCenter}
          >
            {users.map(user => (
              <Marker
                key={user.id}
                lat={user.position.lat}
                lng={user.position.lng}
                user={user}
                openInfoWindow={openInfoWindow}
              />
            ))}

            {selectedUser && (
              <InfoWindow
                lat={selectedUser.position.lat}
                lng={selectedUser.position.lng}
                selectedUser={selectedUser}
                closeInfoWindow={closeInfoWindow}
              />
            )}
          </GoogleMapReact>
        </div>
      )}
      {error && <ErrorComponent />}
    </div>
  );
};

export default memo(Map);
