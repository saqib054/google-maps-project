import React, { memo } from 'react';

const Marker = ({ user, openInfoWindow }) => {
  const markerStyle = {
    width: '5px',
    height: '5px',
    color: 'white',
    borderRadius: '50%',
    padding: '10px',
    display: 'flex',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
    background:
      user.primaryTradeType === 'seller'
        ? 'yellow'
        : user.primaryTradeType === 'buyer'
        ? 'green'
        : user.userTypeIdentifier === 'farmer' && user.primaryTradeType === ''
        ? 'grey'
        : user.userTypeIdentifier === 'trading_house'
        ? 'blue'
        : user.userTypeIdentifier === 'advisor'
        ? 'black'
        : user.userTypeIdentifier === 'mill'
        ? 'orange'
        : 'red'
  };

  const infoWindow = () => {
    openInfoWindow(user);
  };

  return (
    <div style={markerStyle} onClick={infoWindow}>
      <p>{user.listingsCount}</p>
    </div>
  );
};

export default memo(Marker);
