import React, { memo } from 'react';

const CompanyProfileTypes = ({ profileType, types }) => {
  return (
    <div>
      <p>{profileType} : </p>
      {types.map((type, index) => (
        <li key={index}>{type}</li>
      ))}
    </div>
  );
};

export default memo(CompanyProfileTypes);
