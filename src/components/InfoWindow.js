import React, { memo } from 'react';

import CompanyProfileTypes from './CompanyProfileTypes';

const card = {
  width: '200px',
  backgroundColor: 'white',
  border: '1px solid black',
  display: 'flex',
  flexDirection: 'column',
  margin: '0 auto',
  textAlign: 'center',
  cursor: 'default'
};

const companyImage = {
  width: '100%'
};

const container = {
  padding: '5px'
};

const InfoWindow = ({ selectedUser, closeInfoWindow }) => {
  const infoWindow = () => {
    closeInfoWindow(null);
  };
  return (
    <div style={card}>
      <h2>{selectedUser.company.name}</h2>
      {selectedUser.company.logoPath && (
        <img
          src={selectedUser.company.logoPath}
          alt="Logo"
          style={companyImage}
        />
      )}

      <div style={container}>
        <p>{selectedUser.userTypeIdentifier}</p>
        <p>{selectedUser.company.description}</p>
        <div>
          {selectedUser.company.agricultureTypes.length > 0 && (
            <CompanyProfileTypes
              profileType={'Agriculture Type'}
              types={selectedUser.company.agricultureTypes}
            />
          )}
        </div>
        <div>
          {selectedUser.company.productionTypes.length > 0 && (
            <CompanyProfileTypes
              profileType={'Production Type'}
              types={selectedUser.company.productionTypes}
            />
          )}
        </div>
      </div>
      <button onClick={infoWindow}>Close</button>
    </div>
  );
};

export default memo(InfoWindow);
