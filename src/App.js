import React, { memo } from 'react';
import Map from './components/Map';

const App = () => {
  return (
    <div>
      <Map />
    </div>
  );
};

export default memo(App);
